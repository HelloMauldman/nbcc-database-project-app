const express =  require("express");
const mongoose = require("mongoose");

const memberRoutes = require("./routes/memberRoutes");
const userRoutes = require("./routes/userRoutes");

const app = express();
const cors = require("cors");


mongoose.connect("mongodb+srv://admin:admin1234@nbcc.0ta58.mongodb.net/nbccdatabase?retryWrites=true&w=majority")

mongoose.connection.once("open", ()=> console.log("Now connected to MongoDB Atlas"))
//later don't forget to add corsOptions. You'll need this if you want your front  end to gain access to the Back end. 

const corsOptions = {
	origin:["http://localhost:3000","https://nbcc-database-fe.vercel.app"],
	optionsSuccessStatus: 200
}


app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.use("/members",memberRoutes);
app.use("/users", userRoutes);

app.listen(process.env.PORT || 4000, ()=>{
	console.log(`Server is now open at port ${process.env.PORT || 4000}`);
})
