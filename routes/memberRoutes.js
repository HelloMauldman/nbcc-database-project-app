const express = require("express");
const router = express.Router();
const memberController = require("../controllers/memberControllers")
const auth = require("../auth")

router.get("/all", auth.verify, (req, res)=>{
	memberController.getAllMembers().then(result=>res.send(result))
})

router.get("/:memberId", auth.verify, async (req, res, next)=>{
	try{
		await memberController.getMember(req.params.memberId).then(result => res.send(result))
	}
	catch (err) {
		next(err);
	}
	
})

router.post("/", auth.verify, (req, res)=>{
	memberController.addMember(req.body).then(result=>{res.send(result)})
})

router.put("/update/:memberId", auth.verify, (req, res)=>{
	memberController.updateMember(req.params.memberId, req.body).then(result=>{res.send(result)})
})

router.get("/:memberEBId/educ-bg", auth.verify, async (req, res, next) => {
	try{
		await memberController.getMemberEducBG(req.params.memberEBId).then(result=>{res.send(result)})
	}
	catch(err){
		next(err);
	}
})

router.get("/fam-bg/:memberFBId", auth.verify, (req, res) => {
	memberController.getMemberFamBG(req.params.memberFBId).then(result=>{res.send(result)})
})

router.get("/church-inv/:memberCIId", auth.verify, (req, res) => {
	memberController.getMemberChurchInv(req.params.memberCIId).then(result=>{res.send(result)})
})

router.delete("/delete/:memberId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin == false){
		res.send(false);
	}
	else{
		memberController.deleteMember(req.params.memberId).then(result = res.send(result));
	}
})

module.exports = router;