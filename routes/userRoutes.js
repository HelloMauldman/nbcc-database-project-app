const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers")
const auth =  require("../auth")

//  we do not need to create a component for this one because we are handling sensitive data and instead look to see a message that says contact developer if you want access to the data. 
//decided to make the admin the only one to create a user accout here. so that they  won't have to constantly contact me. 
router.post("/register", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}
	else{
	userController.register(req.body).then(result => res.send(result))
	}
});

// router.post("/register", (req, res)=>{
// 	userController.register(req.body).then(result => res.send(result))
// 	}
// });

router.get("/", auth.verify, (req, res)=>{
	// console.log(`${auth.decode(req.headers.authorization).isAdmin} line 11 at userRoutes.js`)
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}
	else{
		userController.getAllUsers().then(result=>res.send(result))
	}
})

router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then(result=>{res.send(result)})
})


router.get("/logs", auth.verify,(req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin === false){
		res.send(false);
	}
	else{
		userController.getAllLogs().then(result => res.send(result))
	}
})


router.post("/logging", auth.verify, (req, res)=>{
	const userId = auth.decode(req.headers.authorization).id;
	userController.userLogs(userId, req.body).then(result=>res.send(result))
})

router.get("/detailLogs", auth.verify, (req, res)=>{
	const userId = auth.decode(req.headers.authorization).id;
	userController.detailLogs(userId).then(result=>res.send(result))
});

router.get("/details", auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);

	userController.getUser({userId:userData.id}).then(result => res.send(result));
})

module.exports = router;