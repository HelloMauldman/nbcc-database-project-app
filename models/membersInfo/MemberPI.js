const mongoose = require("mongoose");

const memberPISchema = new mongoose.Schema({
	createdAt:{
		type:Date,
		default: new Date()
	},
	lastName:{
		type: String,
		required:[true, "Last Name is required."]
	},
	firstName:{
		type: String,
		required:[true, "First name is required."]
	},
	midName:{
		type:String,
		required:[false]
	},
	age:{
		type:Number,
		required:[true, "Age is required."]
	},
	gender:{
		type:String,
		required:[true, "Gender is required."]
	},
	bloodType:{
		type:String,
		required:[true, "Blood Type is Required."]
	},
	nationality:{
		type:String,
		required:[true, "Nationality is required."]
	},
	generation:{
		type:String,
		required:[true, "Generation is required."]
	},
	birthday:{
		type:Date,
		required:[true, "Birthday is required."]
	},
	birthplace:{
		type:String
	},
	region:{
		type:String,
		required:[true, "Region is required."]
	},
	center:{
		type:String
	},
	presentAdd:{
		type:String,
		required:[true, "Present Address required."]
	},
	permanentAdd:{
		type:String
	},
	contactNo:{
		type:String,
		required:[true, "Contact No. is required."]
	},
	email:{
		type:String,
		required:[true, "Email is required."]
	},
	languages:{
		type:String
	},
	memStatus:{
		type:String
	},
	matchingStat:{
		type:String
	},
	blessingDate:{
		type:String
	},
	socialMedia:{
		facebook:{
			type:String
		},
		insta:{
			type:String
		}
	},
	emergencyContacts:[],
	memberEducBackGround:[
		{
			memberEBId:{
				type:String,
				required:[true, "member EducBackGround Id is required."]
			},
			memberEB:{}
		}
	],
	memberFamBackGround:[
		{
			memberFBId:{
				type:String,
				required:[true, "member FamBackGround Id is required."]
			},
			memberFB:{}
		}
	],
	memberChurchInv:[
		{
			memberCIId:{
				type:String,
				required:[true, "member ChurchInvolv Id is required."]
			},
			memberCI:{}
		}
	]
})

module.exports = mongoose.model("MemberPI", memberPISchema)