const mongoose = require("mongoose");

const memberPISchema = new mongoose.Schema({
	mission:[],
	hjyltInternational:[],
	cheongpyeong:[],
	dpWorkshop:[],
	ltc:[],
	youthCampSummit:[],
	plbwLvl1:[],
	plbwLvl2:[],
	summerCamp:[],
	nationalYEWorkshop:[],
	asiaPacificSHYWorkshop:[],
	bcAssembly:[],
	otherEventsNational:[],
	otherEventsInternational:[],
	fridayYouthService:[],
	sundayYouthService:[],
	memberPIId:{
		type:String
	}
})

module.exports = mongoose.model("MemberCI", memberPISchema)