const mongoose = require("mongoose");

const memberEBSchema = new mongoose.Schema({
	daycare:{
		type:String
	},
	preschool:{
		type:String
	},
	elementary:{
		type:String
	},
	juniorHighSchool:{
		type:String
	},
	seniorHighSchool:{
		type:String
	},
	seniorHStrack:{
		type:String
	},
	academicTrack:{
		type:String
	},
	tvlTrack:{
		type:String
	},
	university:{
		type:String
	},
	assocDegree:{
		type:String
	},
	bachelorDegree:{
		type:String
	},
	masterDegree:{
		type:String
	},
	doctoralDegree:{
		type:String
	},
	otherDegree:[],
	employmentStat:{
		type:String
	},
	licenseOrCert:{
		type:String
	},
	position:{
		type:String
	},
	companyName:{
		type:String
	},
	companyAdd:{
		type:String
	},
	department:{
		type:String
	},
	workHistory:{
		type:String

	},
	memberPIId:{
		type:String
	}

})

module.exports = mongoose.model("MemberEB", memberEBSchema)