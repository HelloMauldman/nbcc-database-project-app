const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email:{
		type:String,
		require:[true, "Email is required."]
	},
	password:{
		type:String,
		require:[true, "Password is required."]
	},
	fName:{
		type:String,
		require:[true, "First Name is required."],
	},
	lName:{
		type:String,
		require:[true, "Last Name is required."]
	},
	loggedOn:{
		type: Date,
		default: new Date()
	},
	isAdmin:{
		type:Boolean,
		default:false
	},
	logs:[
		{
			edits:{
				type:String,
				required:[false]
			},
			editedOn:{
				type:Date,
				default: new Date()
			}
		}
	]
})

module.exports = mongoose.model("User", userSchema);