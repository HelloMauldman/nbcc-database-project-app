const mongoose = require("mongoose");

const memberSchema = new mongoose.Schema({
	lastName:{
		type: String,
		required:[true, "Last Name is required."]
	},
	firstName:{
		type: String,
		required:[true, "First name is required."]
	},
	midInit:{
		type:String,
		required:[false]
	},
	age:{
		type:Number,
		required:[true, "Age is required."]
	},
	gender:{
		type:String,
		required:[true, "Gender is required."]
	},
	nationality:{
		type:String,
		required:[true, "Nationality is required."]
	},
	fatherName:{
		type:String,
		required:[true, "Father's name is required."]
	},
	motherName:{
		type:String,
		required:[true, "Mother's name is required."]
	},
	parentBlessing:{
		type:String,
		required:[false]
	},

	address:{
		type:String,
		required:[true, "Address is required."]
	},
	educLvl:{
		type:String,
		required:[true, "educational level is required."]
	},
	institution:{
		type:String,
		required:[true, "institutional name is required."]
	},
	company:{
		type:String,
		requried:[false]
	},
	mobileNo:{
		type:String,
		required:[true, "Mobile no. is required."]
	},
	telephoneNo:{
		type:String,
		required:[false]
	},
	email:{
		type:String,
		required:[true, "Email is required."]
	},
	isActive:{
		type:Boolean,
		default:true
	},
	sibling:[],
	misc:[{
		notes:{
			type:String,
			required:[false]
		}
	}]

})

module.exports = mongoose.model("Member", memberSchema)