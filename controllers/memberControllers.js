const MemberPI = require("../models/membersInfo/MemberPI");
const MemberEB = require("../models/membersInfo/MemberEB");
const MemberFB = require("../models/membersInfo/MemberFB");
const MemberCI = require("../models/membersInfo/MemberCI");


module.exports.getAllMembers = () => {
	return MemberPI.find().then(result=> {return result})
}

module.exports.addMember = (reqBody) =>{
	
	console.log(reqBody)

	const {lastName, firstName, midName, age, gender, nationality, bloodType, generation, birthday, birthplace, region, center, presentAdd, permanentAdd, contactNo, email, languages, memStatus, matchingStat, blessingDate, socialMedia} = reqBody;

	const {daycare, preschool, elementary, juniorHighSchool, seniorHighSchool, seniorHStrack, academicTrack, tvlTrack, university, assocDegree, bachelorDegree, masterDegree, doctoralDegree, otherDegree, employmentStat, licenseOrCert, position, companyName, companyAdd, department, workHistory} = reqBody.memberEducBackground;

	const {father, mother, blessingOrder, birthOrder, siblingName, spouseName} = reqBody.memberFamBackground;


	const {mission, hjyltInternational, cheongpyeong, dpWorkshop, ltc, youthCampSummit, plbwLvl1, plbwLvl2, summerCamp, nationalYEWorkshop, asiaPacificSHYWorkshop, bcAssembly, otherEventsNational, otherEventsInternational, fridayYouthService, sundayYouthService} = reqBody.memberChurchInvolv;


	console.log(father);

	let newMemberPI = new MemberPI({
		lastName:lastName,
		firstName:firstName,
		midName:midName,
		age:age,
		gender: gender,
		nationality:nationality,
		bloodType:bloodType,
		generation:generation,
		birthday:birthday,
		birthplace:birthplace,
		region:region,
		center:center,
		presentAdd:presentAdd,
		permanentAdd:permanentAdd,
		contactNo:contactNo,
		email:email,
		languages:languages,
		memStatus:memStatus,
		matchingStat:matchingStat,
		blessingDate:blessingDate,
		socialMedia:socialMedia
	});

	let newMemberEB = new MemberEB({
		daycare:daycare,
		preschool:preschool,
		elementary:elementary,
		juniorHighSchool:juniorHighSchool,
		seniorHighSchool:seniorHighSchool,
		seniorHStrack:seniorHStrack,
		academicTrack:academicTrack,
		tvlTrack:tvlTrack,
		university:university,
		assocDegree:assocDegree,
		bachelorDegree:bachelorDegree,
		masterDegree:masterDegree,
		doctoralDegree:doctoralDegree,
		otherDegree:otherDegree,
		employmentStat:employmentStat,
		licenseOrCert:licenseOrCert,
		position:position,
		companyName:companyName,
		companyAdd:companyAdd,
		department:department,
		workHistory:workHistory
	})

	let newMemberFB = new MemberFB({
		father:father,
		mother:mother,
		blessingOrder:blessingOrder,
		birthOrder:birthOrder,
		siblingName:siblingName,
		spouseName:spouseName
	})

	let newMemberCI = new MemberCI({
		mission:mission,
		hjyltInternational:hjyltInternational,
		cheongpyeong:cheongpyeong,
		dpWorkshop:dpWorkshop,
		ltc:ltc,
		youthCampSummit:youthCampSummit,
		plbwLvl1:plbwLvl1,
		plbwLvl2:plbwLvl2,
		summerCamp:summerCamp,
		nationalYEWorkshop:nationalYEWorkshop,
		asiaPacificSHYWorkshop:asiaPacificSHYWorkshop,
		bcAssembly:bcAssembly,
		otherEventsNational:otherEventsNational,
		otherEventsInternational:otherEventsInternational,
		fridayYouthService:fridayYouthService,
		sundayYouthService:sundayYouthService
	})

	newMemberEB.save();
	newMemberFB.save();
	newMemberCI.save();


	newMemberPI.memberEducBackGround.push({
	 	memberEBId:newMemberEB._id,
	 	memberEB:newMemberEB
	})
	newMemberPI.memberFamBackGround.push({
		memberFBId:newMemberFB._id,
		memberFB:newMemberFB
	})
	newMemberPI.memberChurchInv.push({
		memberCIId:newMemberCI._id,
		memberCI:newMemberCI

	})


	return newMemberPI.save().then((newMember, error)=>{
		if(error){
			return false;
		}
		else{

			newMemberEB.memberPIId = newMember._id;
			newMemberEB.save();
			newMemberFB.memberPIId = newMember._id;
			newMemberFB.save();
			newMemberCI.memberPIId = newMember._id;
			newMemberCI.save();

			console.log(newMember)

			return true;

		}
	})
}

module.exports.getMember = (memberId)=>{
	return MemberPI.findById(memberId).then(result=>{
		if(result == null){
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.getMemberEducBG = (memberEBId) => {
	return MemberEB.findById(memberEBId).then(result => {
		if(result == null){
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.getMemberFamBG = (memberFBId) => {
	return MemberFB.findById(memberFBId).then(result => {
		if(result == null){
			return false;
		}
		else{
			return result
		}
	})
}

module.exports.getMemberChurchInv = (memberCIId) => {
	return MemberCI.findById(memberCIId).then(result => {
		if(result == null){
			return false
		}
		else{
			return result
		}
	})
}

module.exports.updateMember = (memberId, reqBody) => {

	const {memberEducBackground, memberFamBackground, memberChurchInvolv} = reqBody;

	const {lastName, firstName, midName, age, gender, nationality, bloodType, generation, birthday, birthplace, region, center, presentAdd, permanentAdd, contactNo, email, languages, memStatus, matchingStat, blessingDate, socialMedia} = reqBody;

	const {daycare, preschool, elementary, juniorHighSchool, seniorHighSchool, seniorHStrack, academicTrack, tvlTrack, university, assocDegree, bachelorDegree, masterDegree, doctoralDegree, otherDegree, employmentStat, licenseOrCert, position, companyName, companyAdd, department, workHistory} = memberEducBackground;

	const {father, mother, blessingOrder, birthOrder, siblingName, spouseName} = memberFamBackground;

	const {mission, hjyltInternational, cheongpyeong, dpWorkshop, ltc, youthCampSummit, plbwLvl1, plbwLvl2, summerCamp, nationalYEWorkshop, asiaPacificSHYWorkshop, bcAssembly, otherEventsNational, otherEventsInternational, fridayYouthService, sundayYouthService} = memberChurchInvolv;

	let updateMemberPI = {
		lastName:lastName,
		firstName:firstName,
		midName:midName,
		age:age,
		gender: gender,
		nationality:nationality,
		bloodType:bloodType,
		generation:generation,
		birthday:birthday,
		birthplace:birthplace,
		region:region,
		center:center,
		presentAdd:presentAdd,
		permanentAdd:permanentAdd,
		contactNo:contactNo,
		email:email,
		languages:languages,
		memStatus:memStatus,
		matchingStat:matchingStat,
		blessingDate:blessingDate,
		socialMedia:socialMedia
	}	
	let updateMemberEB = {
		daycare:daycare,
		preschool:preschool,
		elementary:elementary,
		juniorHighSchool:juniorHighSchool,
		seniorHighSchool:seniorHighSchool,
		seniorHStrack:seniorHStrack,
		academicTrack:academicTrack,
		tvlTrack:tvlTrack,
		university:university,
		assocDegree:assocDegree,
		bachelorDegree:bachelorDegree,
		masterDegree:masterDegree,
		doctoralDegree:doctoralDegree,
		otherDegree:otherDegree,
		employmentStat:employmentStat,
		licenseOrCert:licenseOrCert,
		position:position,
		companyName:companyName,
		companyAdd:companyAdd,
		department:department,
		workHistory:workHistory
	}
	let	updateMemberFB = {
		father:father,
		mother:mother,
		blessingOrder:blessingOrder,
		birthOrder:birthOrder,
		siblingName:siblingName,
		spouseName:spouseName
	}
	let updateMemberCI = {
		mission:mission,
		hjyltInternational:hjyltInternational,
		cheongpyeong:cheongpyeong,
		dpWorkshop:dpWorkshop,
		ltc:ltc,
		youthCampSummit:youthCampSummit,
		plbwLvl1:plbwLvl1,
		plbwLvl2:plbwLvl2,
		summerCamp:summerCamp,
		nationalYEWorkshop:nationalYEWorkshop,
		asiaPacificSHYWorkshop:asiaPacificSHYWorkshop,
		bcAssembly:bcAssembly,
		otherEventsNational:otherEventsNational,
		otherEventsInternational:otherEventsInternational,
		fridayYouthService:fridayYouthService,
		sundayYouthService:sundayYouthService
	};


	return MemberPI.findByIdAndUpdate(memberId, updateMemberPI).then( async (result, error)=>{

		if(error){
			return false;
		}
		else{
			const confirmMemberEB =  await MemberEB.findByIdAndUpdate(result.memberEducBackGround[0].memberEBId, updateMemberEB, {returnDocument:"after"}).then((educBG, error)=>{

				if(error){
					return false
				}
				else{
					return educBG
				}
			})
			const confirmMemberFB =  await MemberFB.findByIdAndUpdate(result.memberFamBackGround[0].memberFBId, updateMemberFB, {returnDocument:"after"}).then((famBG, error)=>{
				if(error){
					return false
				}
				else{
					return famBG
				}
			})
			const confirmMemberCI =  await MemberCI.findByIdAndUpdate(result.memberChurchInv[0].memberCIId, updateMemberCI, {returnDocument:"after"}).then((churchInv, error)=>{
				if(error){
					return false
				}
				else{
					return churchInv
				}
			})


			// result.memberEducBackGround.push({
			// 	memberEBId:result.memberEducBackGround[0].memberEBId,
			// 	memberEB: confirmMemberEB
			// })
			// result.memberFamBackGround.push({
			// 	memberFBId:result.memberFamBackGround[0].memberFBId,
			// 	memberFB: confirmMemberFB
			// })
			// result.memberChurchInv.push({
			// 	memberCIId: result.memberChurchInv[0].memberCIId,
			// 	memberCI: confirmMemberCI
			// })

			result.memberEducBackGround[0].memberEB = confirmMemberEB
			result.memberFamBackGround[0].memberFB = confirmMemberFB
			result.memberChurchInv[0].memberCI = confirmMemberCI

			result.save();
			return true;

		}
	})
}

module.exports.deleteMember = (memberId) =>{
		return Member.findByIdAndDelete(memberId).then((result, error)=>{
			if(result == null){
				return false;
			}
			else{
				if(error){
					return false;
				}
				else{
					return true;
				}
			}
		})
}