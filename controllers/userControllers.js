const  User = require("../models/User");
const bcrypt = require("bcryptjs");
const auth = require("../auth")
let salt = bcrypt.genSaltSync(10);

module.exports.register = (reqBody) =>{
	return User.find({email:reqBody.email}).then(result =>{
		if(result.length > 0){
			return false; 
		}
		else{
			let newUser = new User({
				email:  reqBody.email,
				password: bcrypt.hashSync(reqBody.password, salt),
				fName: reqBody.fName,
				lName: reqBody.lName
			});
			return newUser.save().then((user, error)=>{
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		}
	})
}

module.exports.getAllUsers = () =>{
	return User.find({}).then(result=>{
		return result
	})
}

module.exports.loginUser = (body)=>{
	return User.findOne({email:body.email}).then(result=>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(body.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result.toObject())}
			}
			else{
				return false;
			}
		}
	})
}

module.exports.getAllLogs = () =>{
	return User.find({isAdmin:false}).then(users=>{
		let allLogs = [];
		users.forEach(user=>{
			allLogs.push({
				email:user.email,
				fName: user.fName,
				lName: user.lName,
				userId: user._id,
				logs: user.logs
			})
		})
		return allLogs;
	})
}

module.exports.userLogs = (userId, reqBody)=>{
	return User.findById(userId).then(user=>{
		if(user == null){
			return false;
		}
		else{
			user.logs.push(
				{
					edits:reqBody.edits,
				}
			)
			return user.save().then((result, error)=>{
				if(error){
					return false;
				}
				else{
					return result;
				}
			})
		}

	})
}

module.exports.detailLogs = (userId) =>{
	return User.findById(userId).then(user=>{
		if(user === null){
			return false;
		}
		else{
			return user.logs;
		}
	})
}

module.exports.getUser = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.password = undefined;
		return result
	})
}

//Now proceed to actually create the user interface with the use of ReactJS